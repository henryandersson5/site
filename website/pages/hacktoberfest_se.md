# Hacktoberfest

Hacktoberfest är ett globalt evenemang som anordnas varje oktober av Digital Ocean.
Deltagare utmanas att göra 4 stycken merge requests (eller pull requests) för
att ta sitt första steg in i open source-världen. Läs mer om Hacktoberfest på
[hacktoberfest.com](https://hacktoberfest.com/).

2021 höll vi en föreläsning om Hacktoberfest som du kan se nedan, och om du
har några fler frågor är du varmt välkommen på någon av våra meetups, varje
tisdag klockan 17 i Café Java.

- [Hacktoberfest 2021, av Frans Skarman](/static/hacktoberfest-2021.pdf)

Nytt för i år är att vi tänkte samla ihop projekt som medlemmar i LiTHe kod är
delaktiga i, så att du som vill testa på open source har lätt att fråga om
hjälp! Först är alla projekt listade i en tabell med en kort beskrivning, och en
längre beskrivning följer under tabellen.

| Projekt | Kort beskrivning | Språk | Kontakt (Discord) |
|---|---|---|---|
| <hr> | <hr> | <hr> | <hr> |
| [lithekod.se](https://gitlab.com/lithekod/www/site) | lithekod.se | Python | |
| [Kodapan](https://gitlab.com/lithekod/kodapa-2) | Styrelens Discord-bot | Rust | |
| <hr> | <hr> | <hr> | <hr> |
| [Spade](https://spade-lang.org/) | Hårdvaruprogrammeringsspråk | Rust | Frans Skarman (@thezoq2) |
| [Sputnik](https://gitlab.com/sputnik-engine/sputnik) | Spelmotor till Lua | Rust | Gustav Sörnäs (@sornas) |

Om du är medlem i LiTHe kod och vill lägga till ett eget projekt här kan du
antingen öppna en merge request mot [LiTHe kods
hemsida](https://gitlab.com/lithekod/www/site) (kom ihåg att ändra både svenska
och engelska sidan) eller kontakta någon i styrelsen så kan dom lägga till det
åt dig. Se till att lägga till en hacktoberfest-tagg på ditt repository och
skapa issues för saker som någon som är intresserad av att hjälpa till kan börja
med.

## Styrelsens projekt

Dom här projekten underhålls av styrelsen och används för föreningens
verksamhet på något sätt.

### lithekod.se

[GitLab](https://gitlab.com/lithekod/www/site)

Sidan du befinner dig på just nu! Om du ser något du vill ändra så går det
jättebra att skapa en issue och se om det är något styrelsen är intresserad av.

### Kodapan

[GitLab](https://gitlab.com/lithekod/kodapa-2)

Kodapan är styrelsens Discord-bot som i nuläget håller koll på mötesagendor och
påminnelser om styrelsenmöten. Styrelsen skulle vilja att den också kan påminna
om meetups genom att skriva i announcement-kanalen. Det finns också några
interna förbättringar; till exempel kan man förbättra hur kommunikationen med
Google Calendar API fungerar.

## Medlemmars projekt

Dom här projekten underhålls av medlemmar. Ställ gärna frågor i Discord-servern
eller på någon av våra meetups!

### Spade

[GitLab](https://gitlab.com/spade-lang), [spade-lang.org](https://spade-lang.org)

Spade är ett programmeringsspråk för FPGA:er och annan programmerbar hårdvara.
Programmeringsmässigt kan man hjälpa till med kompilatorn och vårt egna
byggsystem (båda skrivna i Rust), men om du har en egen FPGA hemma (som
programmeras med en open source-toolchain) kan du testa att lägga till en
template-konfiguration.

### Sputnik

[GitLab](https://gitlab.com/sputnik-engine/sputnik)

Sputnik är en väldigt ny spelmotor skriven i Rust. Den kompilerar till en
Lua-modul vilket betyder att den såklart går att använda i Lua, men också med
andra språk som antingen kompilerar till Lua eller kan förstå och importera
Lua-moduler. Det finns vääldigt mycket att göra, men jag ser till att det finns
några små och enkla funktioner att implementera. Det går också att skriva några
enkla demo-spel.
