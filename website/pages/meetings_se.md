# Stormöten

Två gånger om året, en gång på våren och en gång på hösten, hålls ett stormöte i
enlighet med stadgarna. På höstmötet gås föregående verksamhetsår igenom med en
verksamhetsberättelse, en ekonomisk berättelse och revisorns granskning av
styrelsens arbete följt av en röstning om ansvarsfrihet. På vårmötet röstas
nästa verksamhetsårs styrelse in. På både höst- och vårmöten kan motioner lyftas
av medlemmar och propositioner av styrelsen.

Tidigare protokoll finns i [arkivet på vår Gitlab](https://gitlab.com/lithekod/stormoten).

Nästa stormöte är höstmötet 2023 som hålls den 21 november 18.15 i Ada Lovelace (ingång B27).
